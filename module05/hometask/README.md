# Module 5 Tasks

## 1. Generate a template to deploy 3 VMs with the following configuration:

- Azure template should consist of 2 json files (template and configuration) +
- Three (3) VMs with different sizing in one Availability set, set up the Load Balancer for VMs with dynamic public IP. +
- VMs must be accessible via LB PIP.  +
- Also all VMs must be accessible via RDC +
- VM's user password must be stored in Azure Vault +

## 2. Create the Images of the deployed VMs and save them in a separate storage account 3h

azcopy.exe