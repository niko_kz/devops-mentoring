# #############################################################################
# Create images after deployment
# #############################################################################
#Clear-Host
#region 0 Connect to the Azure Cloud 
{
    Login-AzureRmAccount
}
#endregion
#region 1 Define Deployment Variables 

    $Module = "module05"
    $ResourceGroupName = "${Module}-iaas"
    $ImagesSA = "imagesofvms"

#endregion
#region 2 Prepare Resorces 

    # Check prepequsites
    if ($env:PROCESSOR_ARCHITECTURE -eq 'AMD64') { 
        $SearchPath = ${env:ProgramFiles(x86)} 
    } else { 
        $SearchPath = ${env:ProgramFiles} 
    }

    $azcopyPath = "$SearchPath\Microsoft SDKs\Azure\AzCopy"
    $azcopy     = "${azcopyPath}\AzCopy.exe"

    if ((Test-Path -Path "${azcopy}" -PathType Leaf) -and ($env:Path -like "*${azcopyPath}*")) {
        Write-host "AzCopy is installed" -BackgroundColor DarkGreen
    } else {
        Write-Warning "Need to install AzCopy (https://docs.microsoft.com/en-us/azure/storage/common/storage-use-azcopy) and add to PATH"
    }

    # Preparing StorageAccount

    $StorageAccountForImages = Get-AzureRmStorageAccount `
        -Name $ImagesSA `
        -ResourceGroupName $ResourceGroupName `
        -ErrorAction SilentlyContinue
    
    if ($? -ne $true) {

        $StorageAccountForImages = New-AzureRmStorageAccount `
            -ResourceGroupName $ResourceGroupName `
            -Name $ImagesSA `
            -SkuName Standard_LRS `
            -Location $ResourceGroupLocation `
            -Kind Storage `
            -Verbose
    }

#endregion
#region 3 Create Images 

    $VMs = Get-AzureRmVM -ResourceGroupName $ResourceGroupName -Status

    foreach ($VM in $VMs) {

        Write-Host ("{0} in {1} processng" -f $VM.Name, $VM.ResourceGroupName) -BackgroundColor DarkGreen

        # -> Sysprep
        if ($VM.Tags['Image.isSysPreped'] -ne $true) {
            if ($VM.PowerState -eq 'VM Running') {
                Write-Host "`t Running sysprep" -BackgroundColor DarkGreen
                Invoke-AzureRmVMRunCommand `
                    -ResourceGroupName $ResourceGroupName `
                    -Name $VM.Name `
                    -CommandId 'RunPowerShellScript' `
                    -ScriptPath "$ScriptPath\scripts\Generalize-VM.ps1" `
                    -Verbose -Confirm:$false

                if ($?) {
                    $VM.Tags['Image.isSysPreped'] = $true
                } else {
                    $VM.Tags['Image.isSysPreped'] = $false
                }

                Set-AzureRmResource -ResourceId $VM.Id -Tag $VM.Tags -Force

            } else {
                Write-Error ("{0} is not started. Please, start it and repeat procedure!" -f $VM.Name)
                break
            }
        } else {
            Write-Host "`t It has already syspreped" -BackgroundColor DarkGreen
        }

        # -> Generalize
        # TODO: Fix getting status about Generalized
        if (($VM.PowerState -ne 'VM generalized') -and ($VM.Tags['Image.isSysPreped'] -eq $true)) {
            Write-Host "`t Deallocating vm" -BackgroundColor DarkGreen
            Stop-AzureRmVM `
                -ResourceGroupName $ResourceGroupName `
                -Name $VM.Name `
                -Force
            
            Write-Host "`t Generalize vm" -BackgroundColor DarkGreen
            Set-AzureRmVM `
                -ResourceGroupName $ResourceGroupName `
                -Name $VM.Name `
                -Generalized `
                -Verbose

        } else {
            Write-Host "`t It has already generalized" -BackgroundColor DarkGreen
        }

        # -> Save
        if ($VM.PowerState -eq 'VM generalized') {
            Write-Host "`t Saving image to ${ImagesSA}" -BackgroundColor DarkGreen
            Save-AzureRmVMImage `
                -VMName $VM.Name `
                -ResourceGroupName $ResourceGroupName `
                -DestinationContainerName $VM.Name `
                -VHDNamePrefix $VM.Name `
                -Overwrite -Verbose

        } else {
            Write-Host "`t The image has been ready saved" -BackgroundColor DarkGreen
        }

        # -> Copy
        # TODO: Improve the checks
        $StorageAccountSource = $VM.StorageProfile.OsDisk.Vhd.Uri.Replace('https://','').split('.')[0]
        $StorageAccountTarget = $ImagesSA

        $KeySource = @(Get-AzureRmStorageAccountKey -Name $StorageAccountSource -ResourceGroupName $ResourceGroupName)[0].Value
        $KeyTarget = @(Get-AzureRmStorageAccountKey -Name $ImagesSA -ResourceGroupName $ResourceGroupName)[0].Value

        . AzCopy.exe `
            /Source:https://$StorageAccountSource.blob.core.windows.net/system/Microsoft.Compute/Images/$VM.Name `
            /SourceKey:$KeySource `
            /Dest:https://$StorageAccountTarget.blob.core.windows.net/system/Microsoft.Compute/Images/$VM.Name `
            /DestKey:$KeyTarget `
            /S
    }

#endregion