
$Force = $true

$Url = 'https://docs.microsoft.com/en-us/azure/storage/common/storage-use-azcopy'
#$Url = 'https://aka.ms/downloadazcopy'
$Output = [System.IO.Path]::GetTempFileName()

if ($Force) {
  Write-Warning '$Force is set, the downloading will be started from scrach!'
  $Output = [System.IO.Path]::GetTempFileName()
} else {
  if (Test-Path -Path $Output -PathType Leaf) {
    Write-Host '$Output already exists. If you want to restart download, change a key $force to $true.'
  } else {
    $Output = [System.IO.Path]::GetTempFileName()
  }
}

Invoke-WebRequest -Uri $Url -OutFile $Output

if (Test-Path -Path $Output -PathType Leaf) {
  Write-Host 'good'

  
} else {
  Write-Host 'dab'
}

