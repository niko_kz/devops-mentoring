# #############################################################################
# Deployment script
# #############################################################################
Clear-Host
#region 0 Connect to the Azure Cloud 
{
  Login-AzureRmAccount
}
#endregion
#region 1 Define Deployment Variables 

  $Module = "module05"
  $ResourceGroupName = "${Module}-iaas"
  $ResourceGroupLocation = "East US"
  $ResourceDeploymentName = "${Module}-iaas-deployment"

  $ScriptPath = "$PSScriptRoot\hometask"
  $TemplateFile = Join-Path -Path "$ScriptPath\azuredeploy" -ChildPath "azuredeploy.json"
  $TemplateParameterFile = Join-Path -Path "$ScriptPath\azuredeploy" -ChildPath "azuredeploy.parameters.1.json"

#endregion
#region 2 Create Resource Group 
{
  New-AzureRmResourceGroup `
    -Name $ResourceGroupName `
    -Location $ResourceGroupLocation `
    -Verbose -Force
}
#endregion
#region 3 Deploy Key Vault 
{
  $VaultName = "${Module}-keyvault"
  $VaultLocation = "East US"

  New-AzureRmKeyVault `
      -Name $VaultName `
      -ResourceGroupName $ResourceGroupName `
      -Location $VaultLocation `
      -EnabledForTemplateDeployment `
      -Verbose

  $SecretName = "AdminPassword"
  $Password = Read-Host -Prompt "Enter the password for VMs:" -AsSecureString

  if ($Password.ToString().Length -ge 8) {
    Set-AzureKeyVaultSecret `
        -VaultName $VaultName `
        -Name $SecretName `
        -SecretValue $Password
  
    $LinkForTemplate = (Get-AzureRmKeyVault -VaultName $VaultName).ResourceId
    Write-Host "Your link to KeyValut: ${LinkForTemplate}" -BackgroundColor DarkMagenta 
  } else {
    Write-Error "Password is too week!"
    break
  }
}
#endregion
#region 4 Deploy Resources 
{
  $AdditionalParameters = @{
    "dscUrl"      = "https://bitbucket.org/niko_kz/devops-mentoring/raw/379443fdb98bc71b9b723bf00d7c33ab77cfa49e/module05/hometask/dsc/WebServer.zip"
    "dscScript"   = "WebServer.ps1"
    "dscFunction" = "WebServer"
  }

  New-AzureRmResourceGroupDeployment `
    -Name $ResourceDeploymentName `
    -ResourceGroupName $ResourceGroupName `
    -TemplateFile $TemplateFile `
    -TemplateParameterFile $TemplateParameterFile `
    @AdditionalParameters `
    -Verbose -Force
}
#endregion