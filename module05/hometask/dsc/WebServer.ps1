﻿configuration WebServer {
    Import-DscResource –ModuleName 'PSDesiredStateConfiguration'

    WindowsFeature IIS 
    {
        Ensure = "Present"
        Name = "Web-Server"
        IncludeAllSubFeature = $true
    }
}