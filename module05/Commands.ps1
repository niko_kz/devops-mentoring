﻿
# Get list of availble Resource Providers
Get-AzureRmResourceProvider |
  Select-Object ProviderNamespace, ResourceTypes |
  Sort-Object ProviderNamespace

# Get list of availble Resource Types of the Provider
Get-AzureRmResourceProvider -ProviderNamespace Microsoft.Compute |
  Select-Object ResourceTypes, Locations |
  Sort-Object ResourceTypes

# ApiVersions
((Get-AzureRmResourceProvider -ProviderNamespace Microsoft.Compute).ResourceTypes | 
    Where-Object { $_.ResourceTypeName -eq 'virtualMachines' }).ApiVersions


# -----------------------------------------------------------------------------
# VM Extensions Version

Add-AzureAccount
Select-AzureSubscription
Get-AzureVMAvailableExtension |
  Select-Object Publisher, ExtensionName, Version |
  Sort-Object ExtensionName
















