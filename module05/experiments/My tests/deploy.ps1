
$DeploymentName = 'azureDeploy'
$AzureResourceGroup = ""
$AdditionalParameters = @{}


New-AzureRmResourceGroupDeployment `
  -Name $DeploymentName `
  -ResourceGroupName $AzureResourceGroup.ResourceGroupName `
  -TemplateFile $PSScriptRoot\azureDeploy.json `
  -TemplateParameterFile $PSScriptRoot\azureDeploy.parameters.json `
  @AdditionalParameters `
  -Verbose -Force