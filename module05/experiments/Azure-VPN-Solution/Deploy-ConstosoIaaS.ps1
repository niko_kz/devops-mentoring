#region Define Deployment Variables 
{
    $ResourceGroupLocation = "East US"
    $ResourceGroupName = "contoso-iaas"
    $ResourceDeploymentName = "contoso-iaas-deployment"
    $TemplateFile = "contosoiaas.json"
    #$Template = Join-Path -Path $PSScriptRoot -ChildPath $TemplateFile
    $Template = 'Q:\Repositories\Mentoring\DevOps_Azure_2018\module05\contoso\ASwithVms\AS.json'

    $Password = "Dragon_64"
    $SecurePassword = $Password | ConvertTo-SecureString -AsPlainText -Force
}
#endregion
#region Create Resource Group 
{
    New-AzureRmResourceGroup `
      -Name $ResourceGroupName `
      -Location $ResourceGroupLocation `
      -Verbose -Force
}
#endregion
#region Deploy Key Vault
{
    $VaultName = "module05-vault"
    $VaultLocation = "East US"

    New-AzureRmKeyVault `
        -Name $VaultName `
        -ResourceGroupName $ResourceGroupName `
        -Location $VaultLocation `
        -EnabledForTemplateDeployment `
        -Verbose

    $SecretName = "AdminPassword"
    $Password = "Dragon_64"
    $SecurePassword = ConvertTo-SecureString -String $Password -AsPlainText -Force

    Set-AzureKeyVaultSecret `
        -VaultName $VaultName `
        -Name $SecretName `
        -SecretValue $SecurePassword

    $LinkForTemplate = (Get-AzureRmKeyVault -VaultName $VaultName).ResourceId
    $LinkForTemplate
}
#endregion
#region Deploy Resources 
{
    $AdditionParameters = @{
        'vmAdminPassword' = $SecurePassword
    }

    New-AzureRmResourceGroupDeployment `
      -Name $ResourceDeploymentName `
      -ResourceGroupName $ResourceGroupName `
      -TemplateFile $Template `
      @AdditionParameters `
      -Verbose -Force
}
#endregion
