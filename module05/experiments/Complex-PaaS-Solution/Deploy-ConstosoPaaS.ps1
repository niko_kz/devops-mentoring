#region Define Deployment Variables 

$ResourceGroupLocation = ""
$ResourceGroupName = "module-05-contoso"
$ResourceDeploymentName = "module-05-paas-deployment"
$TemplateFile = "contosoPaaS.json"
$Template = Join-Path -Path $PSScriptRoot -ChildPath $TemplateFile

#endregion
#region Create Resource Group 

New-AzureRmResourceGroup `
  -Name $ResourceGroupName `
  -Location $ResourceGroupLocation `
  -Verbose -Force

#endregion
#region Deploy Resources 

New-AzureRmResourceGroupDeployment `
  -Name $ResourceDeploymentName `
  -ResourceGroupName $ResourceGroupName `
  -TemplateFile $Template `
  -Verbose -Force

#endregion
