# DevOps Mentoring Program 2018

This is the repository with all tasks and its description of what I have done during **the mentoring program**.

The repository consists of two main branches: `develop` and `master`. It's obvious that, `develop` is used for experimental stuff, and `master` stores stable things for checking by a *mentor*.

---

## Module 01: **GIT Lessons**

Notes about the progress is in the file `module01\notes.md`.

---

## Module 02: **Azure Basics**

---

## Module 03: **Azure PowerShell**

---

## Module 04: **DSC fundamentals**

---

## Module 05: **Azure ARM Templates**

---

## Module 06: **Azure ARM and DSC**

---

## Module 07: **Jenkins**

---

## Module 08: **Microservices fundamentals**

---

## Module 09: **Azure AD**

---

Nikolay Kozhemyak @ EPAM Systems