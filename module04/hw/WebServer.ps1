﻿# https://bitbucket.org/niko_kz/devops-mentoring/src/develop/module04/hw/simpleWebsite.zip

configuration WebServer {
    <#
    param (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({@(Invoke-WebRequest -UseBasicParsing -Uri $_).StatusCode -eq 200})]
        [System.String]
        $UriToZip
    )
    #>

    $UriToZip = 'https://bitbucket.org/niko_kz/devops-mentoring/raw/b19e3b11460e56bcdfa7aaefa7512b00d58d138d/module04/hw/simpleWebsite.zip'

    Import-DscResource –ModuleName 'PSDesiredStateConfiguration'
    Import-DscResource –ModuleName 'xPSDesiredStateConfiguration'

    WindowsFeature IIS 
    {
        Ensure = "Present"
        Name = "Web-Server"
        IncludeAllSubFeature = $true
    }

    xRemoteFile DownloadWebsiteArchive
    {
        Uri = $UriToZip
        DestinationPath = "C:\inetpub\website.zip"
        DependsOn = "[WindowsFeature]IIS" 
    }

    Archive UnzipWebsiteArchive
    {
        DependsOn = "[xRemoteFile]DownloadWebsiteArchive"
        Path = "C:\inetpub\website.zip"        
        Destination = "C:\inetpub\wwwroot"
    }
}