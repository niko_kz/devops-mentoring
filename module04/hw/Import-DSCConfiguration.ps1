﻿
$ResourceGroup     = 'module04-rg'
$AutomationAccount = 'module04-aa'

Get-ChildItem $PSScriptRoot -Filter '*.ps1' | ForEach-Object {
    If (Select-string -path $_.FullName -Pattern '(^[Cc]onfiguration).*' -Quiet) {
        Write-Host ("Importing {0}" -f $_.FullName) -BackgroundColor DarkGreen
        $Configuration = Import-AzureRmAutomationDscConfiguration -SourcePath $_.FullName `
            -Tags @{ imported = 'byScript' } `
            -Force `
            -Published `
            -ResourceGroupName $ResourceGroup `
            -AutomationAccountName $AutomationAccount `
            -Verbose

        Write-Host ("Compiling {0}" -f $_.FullName) -BackgroundColor DarkGreen
        Start-AzureRmAutomationDscCompilationJob -ConfigurationName $Configuration.Name `
            -Verbose `
            -ResourceGroupName $ResourceGroup `
            -AutomationAccountName $AutomationAccount
    }
}