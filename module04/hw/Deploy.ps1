﻿
# -----------------------------------------------------------------------------
# Variables
$Prefix        = "module04"
$ResourceGroup = "${Prefix}-rg"
$Location      = "eastus2"

# -----------------------------------------------------------------------------
# Connect
Connect-AzureRmAccount

# -----------------------------------------------------------------------------
# Create RG
New-AzureRmResourceGroup -Name $ResourceGroup -Location $Location

$StorageAccount = New-AzureRmStorageAccount   -Name "${Prefix}sa" `
                            -SkuName Standard_GRS `
                            -ResourceGroupName $ResourceGroup `
                            -Location $Location

# -----------------------------------------------------------------------------
# Azure Automation
$AutomationAccount = New-AzureRmAutomationAccount   -Name "${Prefix}-aa" `
                                                    -Plan Basic `
                                                    -ResourceGroupName $ResourceGroup `
                                                    -Location $Location

# -----------------------------------------------------------------------------
# VM
$Credentials = Get-Credential -UserName 'devops' -Message 'Please enter creds that will be used on the VM:'
$VMName = 'iis-core'

$VirtualMachine = New-AzureRmVMConfig -Name $VMName -VMSize 'Standard_B1s'

Set-AzureRmVMOperatingSystem -VM $VirtualMachine -Windows -ComputerName $VMName -Credential $Credentials -ProvisionVMAgent $true -WinRMHttp
Set-AzureRmVMSourceImage -VM $VirtualMachine -PublisherName "MicrosoftWindowsServer" -Offer "WindowsServer" -Skus "2016-Datacenter-Server-Core" -Version latest
Set-AzureRmVMBootDiagnostics -VM $VirtualMachine -Enable -ResourceGroupName $ResourceGroup -StorageAccountName "${Prefix}sa"

#New-AzureRmVM -ResourceGroupName $ResourceGroup -Location $Location -Name $VMName -OpenPorts 80,3389 -ImageName '/Subscriptions/2434af2b-bed5-406c-92ad-5382ba506ed6/Providers/Microsoft.Compute/Locations/eastus2/Publishers/MicrosoftWindowsServer/ArtifactTypes/VMImage/Offers/WindowsServer/Skus/2016-Datacenter-Server-Core/Versions/2016.127.20180912' -Credential $Credentials

New-AzureRmVM   -VM $VirtualMachine `
                -ResourceGroupName $ResourceGroup `
                -Location $Location
             

# Find Images

$Publisher = 'MicrosoftWindowsServer'
$Offer     = 'WindowsServer'
$SKU       = '2016-Datacenter-Server-Core'

#Get-AzureRMVMImagePublisher -Location $Location
#Get-AzureRMVMImageOffer -Location $Location -Publisher $Publisher
#Get-AzureRMVMImageSku -Location $Location -PublisherName $Publisher -Offer $Offer
Get-AzureRMVMImage -Location $Location -Publisher $Publisher -Offer $Offer -Sku $SKU

# Subsriptions


$Subscription = Get-AzureRmSubscription | ? { $_.State -eq 'Enabled' } | Out-GridView -PassThru

Select-AzureRmSubscription -