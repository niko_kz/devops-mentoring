
# No Cpode in PSD1 filem only data
# No code allowed

@{
  # Node Specific Data
  AllNodes = @(
    @{NodeName = 'Server01'; Role = "Fileserver"},
    @{NodeName = 'Server02'; Role = "Test"},
    @{NodeName = 'Server03'; Role = "None"}
  )

  # Non-node specific data
  NonNodeData = @{
    Services = "bits", "service"
  }
}