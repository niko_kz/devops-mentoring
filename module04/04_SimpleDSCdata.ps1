﻿#requires -version 4.0

$Nodes = @(
  @{NodeName = 'Server01'; Role = "Fileserver"},
  @{NodeName = 'Server02'; Role = "Test"},
  @{NodeName = 'Server03'; Role = "None"}
)

$Data = @{
  Services = (Get-Service)
}

$ConfigurationData = @{ 
  AllNodes = $Nodes
  NonNodeData = $Data
}

SimpleDSCwithdata -ConfigurationData $ConfigurationData -Output 'SimpleDSCwithdata'