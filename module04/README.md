# Module 4: DSC fundamentals

## DSC Dundamental

Standards: MOF and CIM

* Prevent configuration "drift"
* Separate configuration from inplementation
* Offers "continous" deployment
* Leverage your existing PowerShell skills

Management Framework 4.0

  CIM DSC Namespace

PowerShell remoting must be enabled

Oprional PKI for SSL and certificates (Pull Servers)

PSDesiredStateCOnfiguration Module

```Powershell
# Get all DSC commands
Get-Command -Module PSDesiredStateConfiguration
```

New command type : Configuration

### DSC Architecture

Stages:

Authoring Phase (Этап разработки)

  * Imperative commands
  * Declarative commands
  * Create MOF definitions

Staging Phase (Промежуточная фаза)

  * Declarative MOF staged
  * Configuration calculated per node

"Make it so" phase

  * Declarative configurations implemented trought imperative providers

DSC Models:

  * **Push Model**

    Start-DSCConfiguration

  * **Pull Model**

    Central server (File Share, HTTP/HTTPS)

### DSC Configurations

1. Configuration **keyword**
2. Define node (Managed server)
3. Configure resorces

## DSC Resources

## DSC Configurations

No UNDO command

```powershell
Get-Command -Type Configuration
Restore-DSCConfiguration # Like a "last known good"
Test-DSCConfiguration # Verify last applied configuration

# Use -Verbose
# Cmdlt stops testing after first detected failure

```

Import custom resource outside from $PSHome

## Deploying DSC Configurations

1. Define a config. and load into powershell (execute it)
2. Invoke that config. as name of the config in the powershell to create MOF
3. Start the config on the computer. Start-DscConfiguration -path
4. Can be pushed as many time as needed.

It's important if you use the push method you need to make sure that all modules/resources that you use it's on the remote servers

## Local Configuration Manager

## Trobleshoooting DSC
