#
#
#

# Collection of DSC
Get-DscConfiguration


# Validate
# -Verbose to see details
# Stops testing after first failure
Test-DscConfiguration

# Rollback
# No way to roll back pre-DSC state
Restore-DscConfiguration


####################################
# LCM
Get-DscLocalConfigurationManager

# The DSC "engine"
# Responible for applying

# Change Behavior
# - Use config to apply
# - Set-DSc...
# - Separately from config

# Metadata: *.meta.mof

Set-DscLocalConfigurationManager -ComputerName -Path

# xDSCDiagnotics

install-Module xdscdiagnostics


# Recommendations

Verify DSC Resources

Test deployments

WinEvent queries use and reuse them

Enable Analytics and Debug logs

Watch permissiobns, it runs SYSTEM context

Take advantage of the xDSCDiagnostics module


TEST, PLAN, VERIFY to reduce need for troublooshoting
    Start simple

Read more about it:
    DSC Eventlogs
    DSC Diagnostics
    Get DSC Book free powershell.org


NEXT STEPS
    Use PowerShell daily
    Powershell and DSC, take tasks taht you dont do before