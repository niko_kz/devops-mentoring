﻿#requires -version 4.0

configuration SimpleDSCwithdata {
  # One can evaluate expressions to get the node list
  # E.g: $AllNodes.Where("Role -eq Web").NodeName

  node $AllNodes.NodeName
  {
    File SimpleFolder
    {
      Ensure = 'Present'
      DestinationPath = 'G:\Repositories\Mentoring\devops-mentoring\module04\results'
      Type = 'Directory'
    }

    File SimpleFile
    {
      Ensure = 'Present'
      Contents = "Inline content"
      DestinationPath = 'G:\Repositories\Mentoring\devops-mentoring\module04\results\03_simple.txt'
      Type = 'File'
      DependsOn = '[File]SimpleFolder'
    }
  }

  


}


SimpleDSC