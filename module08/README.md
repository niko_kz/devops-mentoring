# Module 8: Microservices fundamentals

I used the `docker-compose` as the tool to up more that 2 docker images.

## Tasks:

### 1. Install Docker (Locally or on Azure VM)

  - Install Docker For Windows on the local machine.
  - Configure the persistant volumes.

### 2. Install Database Container

  - Get image `docker pull mysql:5.7`
  - Add it to the `docker-compose.yaml`
  - Include the `environment` values to describe passwords.

### 3. Install Web Server Container

  - Get image `docker pull wordpress:latest`
  - Create config file and point to it by volumes.
  - Link with wordpress docker.

### 4. Install Wordpress Container

  - Get image `docker pull nginx:latest`
  - Set the persistant volume for htmls files.
  - Link with db server.

## Result

Functional Wordpress installation on Docker.
