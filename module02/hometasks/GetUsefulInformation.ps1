<#
  Author: Nikolay Kozhemyak
  Module 02 Azure Basics
  Utils
#>

$Location = 'eastus'


# Connection
if ($null -eq $(Get-AzureRmContext).Account) {
    Connect-AzureRmAccount -ErrorAction Stop
}

# Select subscription
Get-AzureRmSubscription | 
    Out-GridView -Title 'Select the subscription' -PassThru | 
    Select-AzureRmSubscription

Get-AzureRmContext

# Get locations
Get-AzureRmLocation | 
    Sort-Object -Property Location | 
    Select-Object -Property Location |
    Format-Wide -Column 2

# Get VM sizes

Get-AzureRmVMSize -Location $Location

Standard_B2s
Standard_DS1_v2