<#
 .SYNOPSIS
    Deploys module 2 solution to Azure

 .AUTHOR
    Nikolay Kozhemyak

 .TASKS
    - Create 2 VMs with different size in one Availability set +
    - Set up the Load Balancer for VMs with dynamic public IP +
    - VMs must be accessible via LB PIP +
    - Also all VMs must be accessible via RDP +

#>
#******************************************************************************
# Variables
$LocationName       	= 'eastus'
$ServicePrefix      		= 'aztraining'
$SelectSubscription 	= $false

# Generic Names
$ResourceGroupName  	= "$ServicePrefix-rg"
$StorageAccountName 	= "$ServicePrefix" + 'store'
$AvailabilySetName  	= "$ServicePrefix-web-as"

# Virual Machines: name=size
$VMs = @{
    'win-web-vm01' = 'Standard_B2s'
    'win-web-vm02' = 'Standard_DS1_v2'
	
}

#******************************************************************************
# Conncections/subscription

if ($(Get-AzureRmContext).Account -eq $null) {
    Write-host "Connecting ..." -BackgroundColor DarkGreen
    $Account = Connect-AzureRmAccount -ErrorAction Stop
}

if ($SelectSubscription) {
    Write-host "Selecting subscription ..." -BackgroundColor DarkGreen
    Get-AzureRmSubscription |
        Out-GridView -PassThru -Title 'Select one item from list and click OK' | 
        Select-AzureRmSubscription
}

#******************************************************************************
# Creation process
# -----------------------------------------------------------------------------
# ResourceGroup & StorageAccount

Write-host "Creating ResourceGroup $ResourceGroupName ..." -BackgroundColor DarkGreen
Get-AzureRmResourceGroup -Name $ResourceGroupName -Location $LocationName -ErrorAction SilentlyContinue

if (!$?) {
    New-AzureRmResourceGroup -Name $ResourceGroupName -Location $LocationName -ErrorAction Stop
}

Write-host "Creating StorageAccount $StorageAccountName ..." -BackgroundColor DarkGreen
$StorageAccount = New-AzureRmStorageAccount -ResourceGroupName $ResourceGroupName `
                                            -Name $StorageAccountName `
                                            -SkuName Standard_LRS -Kind Storage `
                                            -Location $LocationName `
                                            -ErrorAction SilentlyContinue

Set-AzureRmCurrentStorageAccount -ResourceGroupName $ResourceGroupName `
                                 -AccountName $StorageAccountName `
                                 -ErrorAction Stop

# -----------------------------------------------------------------------------
# Virtual Network & Subnets
# -----------------------------------------------------------------------------
Write-host "Creating VNet & WebSubnet..." -BackgroundColor DarkGreen
$WebSubnet = New-AzureRmVirtualNetworkSubnetConfig  -Name 'webservers' `
                                                    -AddressPrefix 192.168.10.0/24

$VNet = New-AzureRmVirtualNetwork -Name "$ServicePrefix-vnet" `
                                  -ResourceGroupName $ResourceGroupName `
                                  -Location $LocationName `
                                  -AddressPrefix 192.168.0.0/16 `
                                  -Subnet $WebSubnet

# -----------------------------------------------------------------------------
# Security (NSG, Rules)
# -----------------------------------------------------------------------------
Write-host "Creating NSG & Rules..." -BackgroundColor DarkGreen
$NSGRule_RDP = New-AzureRmNetworkSecurityRuleConfig -Name 'RDP-Rule' `
                                              -Description "Allow RDP" `
                                              -Access Allow -Protocol Tcp `
                                              -Direction Inbound `
                                              -Priority 100 `
                                              -SourceAddressPrefix Internet `
                                              -SourcePortRange * `
                                              -DestinationAddressPrefix * `
                                              -DestinationPortRange 3389

# I think it's not necessary fo LB. It opens by default.
$NSGRule_HTTP = New-AzureRmNetworkSecurityRuleConfig -Name 'WEB-Rule' `
                                              -Description "Allow HTTP" `
                                              -Access Allow -Protocol Tcp `
                                              -Direction Inbound `
                                              -Priority 110 `
                                              -SourceAddressPrefix Internet `
                                              -SourcePortRange * `
                                              -DestinationAddressPrefix * `
                                              -DestinationPortRange 80

$NSG = New-AzureRmNetworkSecurityGroup  -ResourceGroupName $ResourceGroupName `
                                        -Location $LocationName `
                                        -Name "$ServicePrefix-web-nsg" `
                                        -SecurityRules $NSGRule_RDP, $NSGRule_HTTP

# -----------------------------------------------------------------------------
# Availability Set
# -----------------------------------------------------------------------------
Write-host "Creating Availability Set $AvailabilySetName ..." -BackgroundColor DarkGreen
$AvailabilySet = New-AzureRmAvailabilitySet -Name $AvailabilySetName `
                                            -PlatformFaultDomainCount 2 `
                                            -ResourceGroupName $ResourceGroupName `
                                            -Location $LocationName `
                                            -Sku Aligned

# -----------------------------------------------------------------------------
# External Load Balancer
# -----------------------------------------------------------------------------
Write-host "Create new public IP for LB..." -BackgroundColor DarkGreen
$LB_PublicIP = New-AzureRmPublicIpAddress  -Name ("$ServicePrefix" + 'elb_pip') `
                                           -ResourceGroupName $ResourceGroupName `
                                           -Location $LocationName `
                                           -AllocationMethod Dynamic

Write-host "Preparing configuration for LB..." -BackgroundColor DarkGreen
$LB_Frontend = New-AzureRmLoadBalancerFrontendIpConfig -Name "FrontEnd" -PublicIpAddress $LB_PublicIP
$LB_backendAddressPool = New-AzureRmLoadBalancerBackendAddressPoolConfig -Name "WebServers"

# RDP Probe and Rule
$LB_ProbeCheck01 = New-AzureRmLoadBalancerProbeConfig -Name "rdp-check" `
                                                    -Protocol "tcp" -Port 3389 `
                                                    -IntervalInSeconds 15 `
                                                    -ProbeCount 2

$LB_Rule01 = New-AzureRmLoadBalancerRuleConfig -Name "rdp-random" `
                                             -FrontendIPConfiguration $LB_Frontend `
                                             -BackendAddressPool $LB_backendAddressPool `
                                             -Probe $LB_ProbeCheck01 `
                                             -Protocol "Tcp" -FrontendPort 3389 -BackendPort 3389 `
                                             -IdleTimeoutInMinutes 15 -LoadDistribution Default

# HTTP Probe and Rule
$LB_ProbeCheck02 = New-AzureRmLoadBalancerProbeConfig -Name "http-check" `
                                                    -Protocol "tcp" -Port 80 `
                                                    -IntervalInSeconds 15 `
                                                    -ProbeCount 2

$LB_Rule02 = New-AzureRmLoadBalancerRuleConfig -Name "http-random" `
                                             -FrontendIPConfiguration $LB_Frontend `
                                             -BackendAddressPool $LB_backendAddressPool `
                                             -Probe $LB_ProbeCheck02 `
                                             -Protocol "Tcp" -FrontendPort 80 -BackendPort 80 `
                                             -IdleTimeoutInMinutes 15 -LoadDistribution Default

Write-host "Create new LB ..." -BackgroundColor DarkGreen
$LB = New-AzureRmLoadBalancer  -Name ("$ServicePrefix" + 'elb') `
                               -ResourceGroupName $ResourceGroupName `
                               -Location $LocationName `
                               -FrontendIpConfiguration $LB_Frontend `
                               -BackendAddressPool $LB_backendAddressPool `
                               -Probe $LB_ProbeCheck01, $LB_ProbeCheck02 `
                               -LoadBalancingRule $LB_Rule01, $LB_Rule02

# -----------------------------------------------------------------------------
# Virtual Machines (Set of them)
# -----------------------------------------------------------------------------

Write-host "Getting new credentials..." -BackgroundColor DarkGreen
$Credentials = Get-Credential -Message "Enter a new username and password for virtual machines:"

# Create VMs
foreach ($VM_Name in $VMs.Keys) {
    Write-host "-------------------------------" -BackgroundColor DarkGreen
    $VM_Size = $VMs.Item($VM_Name)

    Write-host "$VM_Name is processing..." -BackgroundColor DarkGreen

    Write-host "Get subnet id..." -BackgroundColor DarkGreen
    $WebSubnet_ID = ($VNet | Get-AzureRmVirtualNetworkSubnetConfig -name $WebSubnet.Name).id

    Write-host "Create new public IP..." -BackgroundColor DarkGreen
    $PublicIP = New-AzureRmPublicIpAddress  -Name ($VM_Name + '_pip') `
                                            -ResourceGroupName $ResourceGroupName `
                                            -Location $LocationName `
                                            -AllocationMethod Dynamic

    Write-host "Create new NIC..." -BackgroundColor DarkGreen
    $NIC = New-AzureRmNetworkInterface  -Name ($VM_Name + '_nic') `
                                        -ResourceGroupName $ResourceGroupName `
                                        -Location $LocationName `
                                        -SubnetId $WebSubnet_ID `
                                        -PublicIpAddressId $PublicIP.Id `
                                        -LoadBalancerBackendAddressPoolId $LB_backendAddressPool.Id


    Write-host "Set NSG to NIC..." -BackgroundColor DarkGreen
    $NIC.NetworkSecurityGroup = $NSG
    Set-AzureRmNetworkInterface -NetworkInterface $NIC

    Write-host "Create config for new VM..." -BackgroundColor DarkGreen
    $VirtualMachine = New-AzureRmVMConfig  -VMName $VM_Name `
                                           -VMSize $VM_Size `
                                           -AvailabilitySetId $AvailabilySet.id

    Write-host "Configure new VM..." -BackgroundColor DarkGreen
    Set-AzureRmVMOperatingSystem -VM $VirtualMachine -Windows -ComputerName $VM_Name -Credential $Credentials -ProvisionVMAgent $true
    Set-AzureRmVMSourceImage -VM $VirtualMachine -PublisherName MicrosoftWindowsServer -Offer WindowsServer -Skus 2016-Datacenter -Version latest
    Add-AzureRmVMNetworkInterface -VM $VirtualMachine -Id $NIC.Id

    Write-host "Create new VM..." -BackgroundColor DarkGreen
    New-AzureRmVM -ResourceGroupName $ResourceGroupName `
                  -Location $LocationName `
                  -VM $VirtualMachine `
                  -DisableBginfoExtension
}

# -----------------------------------------------------------------------------
# DSC Extension (IIS and Simple HTML)
# -----------------------------------------------------------------------------

Write-host "Publish DSC for VMs..." -BackgroundColor DarkGreen
#.\dsc\install-iis.ps1 `
$PublishedDSC = Publish-AzureRmVMDscConfiguration -ConfigurationPath  'G:\Repositories\Mentoring\devops-mentoring\module02\hometasks\dsc\Install-IIS.ps1' `
                                            -ResourceGroupName $ResourceGroupName `
                                            -StorageAccountName $StorageAccountName `
                                            -force

foreach ($VM_Name in $VMs.Keys) {

    Write-host "Set some stuff for IIS trough DSC..." -BackgroundColor DarkGreen
    $DSCParameters = @{ 
        'ServerName' = $VM_Name
        'ResourceGroup' = $ResourceGroupName 
    }

    Set-AzureRmVMDscExtension -Version '2.76' `
                            -ResourceGroupName $ResourceGroupName `
                            -VMName $VM_Name `
                            -ArchiveStorageAccountName $StorageAccountName `
                            -ArchiveBlobName "Install-IIS.ps1.zip" `
                            -AutoUpdate `
                            -ConfigurationArgument $DSCParameters 
}

# -----------------------------------------------------------------------------
# Finish!

Write-host "Done!" -BackgroundColor DarkGreen
Write-host "-------------------------------" -BackgroundColor DarkGreen

break