configuration Install-IIS.ps1
{
    param(
        [Parameter(Mandatory=$true)]
        [string] $ServerName,

        [Parameter(Mandatory=$true)]
        [string] $ResourceGroup
    )


    node "localhost"
    {
        WindowsFeature IIS
        {
            Ensure = "Present"
            Name = "Web-Server"
        }

        File CreateIndex
        {
            DestinationPath = "C:\inetpub\wwwroot\index.html"
            Type = "File"
            Contents = @"
<!doctype html>
<html lang="en">
  <meta charset="utf-8">
  <head>
  <title>$ServerName</title>
  <style type="text/css">
    body {
      padding: 0;
      margin: 0;
      background: #453F78;
      font-family: 'Arial';
      text-align: center;
      text-shadow: 2px 2px 8px #401F3E;
    }
    h1 {
      color: #FAF2A1;
      position: absolute;
      padding: 0;
      margin: 0;
      top: 50%;
      left: 50%;
      transform: translateX(-50%) translateY(-50%);
    }
    p {
      color: #759AAB;
      position: absolute;
      margin: 10;
      top: 50%;
      left: 50%;
      transform: translateX(-50%);
    }
  </style>
</head>
<body>
  <h1>$ServerName at $ResourceGroup</h1>
  <p>Kozhemyak Nikolay @ Module 02 Azure Basics</p>
</body>
</html>
"@
            Force = $true
            DependsOn = "[WindowsFeature]IIS"
        }
    }
}
