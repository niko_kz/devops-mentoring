# Module 06: ARM fundamentals

## Mission

1.	Create ARM template that deploy 2 VM (Windows Server 2012R2) under LoadBalancer and all needed infrastructure.
2.	Configure Windows Server:  install IIS server by using DSC extension
3.	By using DSC deploy a dummy website on each server (plain htm). Site on Server #1 should display “hello from the bottom of my site1”. Site on Server #2 should display “hello from the bottom of my site2”
4.	Push everything (scripts/configs) in repo

---
## Develop

### 1. Create ARM template that deploy 2 VM

I took `ARM Template` from the previous module. It perfectly fits. Only thing i needed to change was `OS version` and the `DSC template`.





---
Nikolay Kozhemyak @ EPAM Systems