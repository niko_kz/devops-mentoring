﻿configuration WebServer
{
	param ($Index, $ModuleName)

    Import-DscResource –ModuleName 'PSDesiredStateConfiguration'
	Import-DscResource –ModuleName 'xPSDesiredStateConfiguration'
	Import-DscResource –ModuleName 'xWebAdministration'

	Node "localhost"
    {
		WindowsFeature IIS 
		{
			Ensure = "Present"
			Name = "Web-Server"
			IncludeAllSubFeature = $true
        }
        
        File Index.Html {
            DependsOn = @("[WindowsFeature]IIS", "[xEnvironment]WebsiteIndex")
            DestinationPath = "C:\inetpub\wwwroot\index.html"
            Contents = `
@"
<!DOCTYPE HTML>
<html>
	<head>
	    <title>Mentoring Program</title>
		<meta charset="utf-8" />
	</head>
	<body>
        <h1>Hello from the my WebSite $Index</h1>
        <p>The hometask by Kozhemyak Nikolay for $ModuleName.</p>
	</body>
</html>
"@
            
        }
        
        xEnvironment WebsiteIndex
        {
            Name = 'WEBSITE_INDEX'
            Value = $VMName
        }
        <#
        xRemoteFile DownloadWebsiteArchive
        {
            Uri = $UriToZip
            DestinationPath = "C:\inetpub\website.zip"
            DependsOn = "[WindowsFeature]IIS" 
        }

        Archive UnzipWebsiteArchive
        {
            DependsOn = "[xRemoteFile]DownloadWebsiteArchive"
            Path = "C:\inetpub\website.zip"        
            Destination = "C:\inetpub\wwwroot"
        }
        #>
	}
}