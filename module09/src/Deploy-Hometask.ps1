###############################################################################
# Variables
$ResourceGroupName  = "module09ps"
$Location           = "westus"
$TenatID            = '54c8aa39-d5a3-4ec7-a01e-8ab24ce1e74e' # DicrectoryID, you can find it on the portal
$AdminUser          = 'devopskz_gmail.com#EXT#@devopskzgmail.onmicrosoft.com'
$GroupName          = "Administrators"
$ManagedDomainName  = "module09.com"
$VnetName           = "DomainServices"

###############################################################################
#region Login to the portals (Select and type F8)
{
  Write-Host "Connecting to Azure..." -BackgroundColor DarkMagenta
  # Connect to your Azure AD directory.
  Connect-AzureAD -TenantId $TenatID

  # Login to your Azure subscription.
  Connect-AzureRmAccount
  $Context = Get-AzureRmContext
  $AzureSubscriptionId = $Context.Subscription.Id
}
#endregion
#region Primary configuration
  Write-Host "Creating group '$GroupName' and add user '$AdminUser' to Azure..." -BackgroundColor DarkMagenta
  # Create the service principal for Azure AD Domain Services.
  New-AzureADServicePrincipal -AppId "2565bd9d-da50-47d4-8b85-4c97f669dc36" -ErrorAction SilentlyContinue

  # Create group and add user
  if (@(Get-AzureADGroup -Filter "DisplayName eq '${GroupName}'").count -eq 0) {
    Write-Host "Creating group '$GroupName'" -BackgroundColor DarkCyan
    New-AzureADGroup -DisplayName $GroupName `
      -Description "Group to fit the hometask requirement of module 09" `
      -SecurityEnabled $true -MailEnabled $false `
      -MailNickName $GroupName
  }

  $GroupId = Get-AzureADGroup -Filter "DisplayName eq '${GroupName}'" | `
    Select-Object -ExpandProperty ObjectId

  $UserId = Get-AzureADUser -Filter "UserPrincipalName eq '$AdminUser'" | `
    Select-Object -ExpandProperty ObjectId
  
  if ($null -notin @($GroupId, $UserId)) {
    Write-Host "Adding user '$AdminUser'" -BackgroundColor DarkCyan
    Add-AzureADGroupMember -ObjectId $GroupId -RefObjectId $UserId -ErrorAction SilentlyContinue
  } else {
    Write-Error "${GroupId} and ${UserId} are empty"
    break
  }
#endregion
#region Create Azure AD Services
  Write-Host "Configuring Azure AD Services..." -BackgroundColor DarkMagenta

  # Register the resource provider for Azure AD Domain Services with Resource Manager.
  Register-AzureRmResourceProvider -ProviderNamespace Microsoft.AAD

  # Create the resource group.
  Write-Host "New Resource Group" -BackgroundColor DarkMagenta
  New-AzureRmResourceGroup `
    -Name $ResourceGroupName `
    -Location $Location

  # Create the dedicated subnet for AAD Domain Services.
  Write-Host "New vNet" -BackgroundColor DarkMagenta
  $DomainSubnet = New-AzureRmVirtualNetworkSubnetConfig `
    -Name DomainServices `
    -AddressPrefix 10.0.0.0/24

  # Create the virtual network in which you will enable Azure AD Domain Services.
  New-AzureRmVirtualNetwork `
    -ResourceGroupName $ResourceGroupName `
    -Location $Location `
    -Name $VnetName `
    -AddressPrefix 10.0.0.0/16 `
    -Subnet $DomainSubnet

  # Enable Azure AD Domain Services for the directory.
  Write-Host "Enable Azure AD Domain Services" -BackgroundColor DarkMagenta
  
  $ResourceId = "/subscriptions/$AzureSubscriptionId/resourceGroups/$ResourceGroupName/providers/Microsoft.AAD/DomainServices/$ManagedDomainName"
  $SubnetId   = "/subscriptions/$AzureSubscriptionId/resourceGroups/$ResourceGroupName/providers/Microsoft.Network/virtualNetworks/$VnetName/subnets/DomainServices"

  New-AzureRmResource `
    -ResourceId ResourceId `
    -Location $Location `
    -Properties @{
      "DomainName" = $ManagedDomainName
      "SubnetId"   = $SubnetId
    } `
    -ApiVersion 2017-06-01 `
    -Force `
    -Verbose

#endregion