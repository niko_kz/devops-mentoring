# #############################################################################
# Deployment script
# #############################################################################
Clear-Host
#region 0 Connect to the Azure Cloud 
{
  Login-AzureRmAccount
}
#endregion
#region 1 Define Deployment Variables 

  # Unique label
  $Module                 = "module06"

  # Resource Labels
  $ResourceGroupName      = "${Module}-dsc"
  $ResourceGroupLocation  = "East US"
  $ResourceDeploymentName = "${ResourceGroupName}-deployment"

  # ARM Templates
  if ($psISE) {
    $PathToScript = Split-Path -Path $psISE.CurrentFile.FullPath        
  } else {
    $PathToScript = $global:PSScriptRoot
  }

  $TemplateFile           = Join-Path -Path "${PathToScript}\azuredeploy" -ChildPath "azuredeploy.json"
  $TemplateParameterFile  = Join-Path -Path "${PathToScript}\azuredeploy" -ChildPath "azuredeploy.parameters.json"

#endregion
#region 2 Create Resource Group 
{
  New-AzureRmResourceGroup `
    -Name $ResourceGroupName `
    -Location $ResourceGroupLocation `
    -Verbose -Force
}
#endregion
#region 3 Deploy Key Vault 
{
  $VaultName = "${Module}-keyvault"
  $VaultLocation = "East US"

  New-AzureRmKeyVault `
      -VaultName $VaultName `
      -ResourceGroupName $ResourceGroupName `
      -Location $VaultLocation `
      -EnabledForTemplateDeployment `
      -Verbose

  # FYI Set-AzureRmKeyVaultAccessPolicy -VaultName $keyVaultName -UserPrincipalName $userPrincipalName -PermissionsToSecrets set,delete,get,list

  $SecretName = "AdminPassword"
  $Password = Read-Host -Prompt "Enter the password for VMs:" -AsSecureString

  if ($Password.ToString().Length -ge 8) {
    Set-AzureKeyVaultSecret `
        -VaultName $VaultName `
        -Name $SecretName `
        -SecretValue $Password
  
    $Link = (Get-AzureRmKeyVault -VaultName $VaultName).ResourceId
    Write-Host "Your link to KeyValut:`n$Link" -BackgroundColor DarkMagenta 
  } else {
    Write-Error "Password is too week!"
    break
  }
}
#endregion
#region 4 Deploy Resources 
{
  <#
  $DSCParameters = @{
    "dscUrl"      = "https://bitbucket.org/niko_kz/devops-mentoring/raw/379443fdb98bc71b9b723bf00d7c33ab77cfa49e/module05/hometask/dsc/dsc.zip"
    "dscScript"   = "WebServer.ps1"
    "dscFunction" = "WebServer"
  }
  #>

  New-AzureRmResourceGroupDeployment `
    -Name $ResourceDeploymentName `
    -ResourceGroupName $ResourceGroupName `
    -TemplateFile $TemplateFile `
    -TemplateParameterFile $TemplateParameterFile `
    -Verbose -Force
}
#endregion