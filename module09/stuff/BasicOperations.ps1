﻿{
    Find-Module AzureAD
}

{
    Install-Module AzureAD
}

{
    Import-Module AzureAD
}

{
    $TenatID = '54c8aa39-d5a3-4ec7-a01e-8ab24ce1e74e'
    $AzureADCredentials = Get-Credential -Message "Creds to connect to Azure AD:"
    Connect-AzureAD -TenantId $TenatID #-Credential $AzureADCredentials
}

{
    Get-AzureADCurrentSessionInfo
}

{
    Get-AzureADTenantDetail
}

{
    # Create the service principal for Azure AD Domain Services.
    New-AzureADServicePrincipal -AppId "2565bd9d-da50-47d4-8b85-4c97f669dc36"

    # Create the delegated administration group for AAD Domain Services.
    New-AzureADGroup -DisplayName "AAD DC Administrators (PS)" `
      -Description "Delegated group to administer Azure AD Domain Services" `
      -SecurityEnabled $true -MailEnabled $false `
      -MailNickName "AADDCAdministrators"

    Get-AzureADServicePrincipal


    # Register the resource provider for Azure AD Domain Services with Resource Manager.
    Register-AzureRmResourceProvider -ProviderNamespace Microsoft.AAD
}
