﻿# Check Folders
Write-Host "---`n- Verify parameters"
$env:FolderToBackup, $env:BackupLocation | ForEach-Object {
    if (Test-Path -Path $_ -Type Container) {
        Write-Host "$_ exist"
    } else {
        Write-Error "ERROR: '$_' doesn't exist"
        break
    }
}

if ($env:FolderToBackup -eq $env:BackupLocation) {
    Write-Error "ERROR: Both path are the same!"
    break
}

# Create Backup Folder
Write-Host "---`n- Create Backup Folder"
$BackupFolder = "bak-{0}-{1}" -f (Get-Date -Format 'yyyyMMddhhmmss'), $ENV:BUILD_NUMBER
$BackupPath   = Join-Path -Path $env:BackupLocation -ChildPath $BackupFolder

if (-not(Test-Path -Path $BackupPath -Type Container)) {
    New-Item -Path $BackupPath -ItemType Directory -Force | Out-Null
}

# Backup Files
Write-Host "---`n- Do copy process"
Copy-Item `
    -Path $env:FolderToBackup `
    -Destination $BackupPath `
    -Container `
    -Recurse `
    -Force

# For Test
Write-Host "---`n- Add metadata file"
Add-Content -Path "${BackupPath}\_jobinfo.txt" -Value ("DATE: {0}" -f (Get-Date))
Add-Content -Path "${BackupPath}\_jobinfo.txt" -Value "BUILD_NUMBER: ${ENV:BUILD_NUMBER}"
Add-Content -Path "${BackupPath}\_jobinfo.txt" -Value "JOB_NAME: ${ENV:JOB_NAME}"
Add-Content -Path "${BackupPath}\_jobinfo.txt" -Value "JENKINS_URL: ${ENV:JENKINS_URL}"
Add-Content -Path "${BackupPath}\_jobinfo.txt" -Value "BUILD_URL: ${ENV:BUILD_URL}"