# Module 03: **Azure PowerShell**

## Tasks Progress

| # | Task | Status |
| - | ------------------ | ------ |
| 1 | Powershell installation | :white_check_mark: |
| 2 | Working with CSV Data in PowerShell | :white_check_mark: |
| 3 | Managing Azure IaaS with PowerShell | :white_check_mark: |
| 4 | Microsoft Azure Automation | :white_check_mark: |
| 5 | PowerShell operator -f | :white_check_mark: |

## How to check the PowerShell version & install a new version

Details are [here](https://mikefrobbins.com/2015/01/08/how-to-check-the-powershell-version-and-install-a-new-version/) and [here](https://4sysops.com/wiki/differences-between-powershell-versions/) (new).

```PowerShell
# Get PS Version
# Just type $psv + tab
$PSVersionTable

# Check net 4 version
(Get-ItemProperty -Path 'HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4\Full' -ErrorAction SilentlyContinue).Version

# Use downgrade version, but it requires the .NET 2.0, 3.5, etc
PowerShell.exe -Version 2

# check whether you are in 32-bit or 64-bit shell
[Environment]::Is64BitProcess

```

## Get started with PowerShell development in Visual Studio Code

https://trevorsullivan.net/2017/02/24/use-visual-studio-code-write-powershell/

* `Ctrl + Shift + P` or `F1` to invoke Command Palette.
* `Ctrl + P` - switching to another file.

To setup rules of the `PSScriptAnalyzer`, open the command and type PSScriptAnalyzer and select what you want.



## Use Splatting to Simplify Your PowerShell Scripts

It's nothing special, but can help to simplify look of total commands.

* Splatting
* Proxy Commands
* Command MetaData

` - Backtick.

```PowerShell
# Hash Table
$MailMessage = @{ 
    To = "me@mycompany.com" 
    From = "me@mycompany.com" 
    Subject = "Hi" 
    Body = "Hello" 
    Smtpserver = "smtphost" 
    ErrorAction = "SilentlyContinue" 
}

# Use hash tables as parameter presets
Send-MailMessage @MailMessage
```

## Managing Azure IaaS with PowerShell

Service Level Agreements provides 99.95% with cavets.


