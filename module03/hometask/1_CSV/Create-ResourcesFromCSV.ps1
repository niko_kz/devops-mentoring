﻿<#
 .SYNOPSIS
 Create ResurceGroup and deploy ARM Template

 .PROGRAM
 DevOps Mentoring Program. Azure Stream #3 (KZ)

 .MODULE 03
 Azure PowerShell

 .TASK
 Create a CSV file and load all the resources in a subscription into it using PowerShell script.

 .AUTHOR
 Nikolay Kozhemyak
#>

[CmdletBinding()]

Param (
    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [String] $ResourceGroup,

    [Parameter(Mandatory=$false)]
    [Switch] $Select
)

$VerbosePreference = 'Continue'
$PredifinedProperties = @('Name','ResourceType','Location')

# -----------------------------------------------------------------------------
# Conncections/subscription

if ($(Get-AzureRmContext).Account -eq $null) {
  Write-Verbose "Connecting to Azure Account"
  Connect-AzureRmAccount -ErrorAction Stop
}

# -----------------------------------------------------------------------------
# Resource Group

$RG = Get-AzureRmResourceGroup -Name $ResourceGroup -ErrorAction Stop

# -----------------------------------------------------------------------------
# Preparation

$Resources = Get-AzureRmResource | Where-Object { $_.ResourceGroupName -eq $ResourceGroup } 

If ($Select) {
    $Properties = $Resources | gm -MemberType NoteProperty | 
        Select-Object -ExpandProperty Name | 
        Out-GridView -PassThru
} else {
    $Properties = $PredifinedProperties
}

# -----------------------------------------------------------------------------
# Formats

$FilePath = "$PSScriptRoot\Output.{0}"

$Resources = $Resources | 
    Select-Object -Property $Properties |
    Sort-Object

##### TXT
$Format = 'txt'
$Resources | 
    Select-Object -ExpandProperty Name | 
    Out-File -FilePath ($FilePath -f $Format) -Encoding utf8

Write-host "Format[$Format]:" -BackgroundColor DarkGreen
Get-Content -Path ($FilePath -f $Format)

# CSV
$Format = 'csv'
$Resources | 
    Export-Csv -Path ($FilePath -f $Format) -NoTypeInformation -Encoding UTF8 -Delimiter ','

Write-host "Format[$Format]:" -BackgroundColor DarkGreen
Import-Csv -Path ($FilePath -f $Format) -Delimiter ',' | Format-Table

# JSON
$Format = 'json'
$Resources | 
    ConvertTo-Json | 
    Out-File -FilePath ($FilePath -f $Format) -Encoding utf8

Write-host "Format[$Format]:" -BackgroundColor DarkGreen
Get-Content -Path ($FilePath -f $Format) | ConvertFrom-Json | Format-Table

# HTML
$Format = 'html'
$Resources | 
    ConvertTo-Html | 
    Out-File -FilePath ($FilePath -f $Format) -Encoding utf8

Write-host "Format[$Format]: It was one direction transmision" -BackgroundColor DarkGreen


# Clixml
$Format = 'clixml'
$Resources | 
    Export-Clixml -Path ($FilePath -f $Format) -Encoding UTF8

Write-host "Format[$Format]:" -BackgroundColor DarkGreen
Import-Clixml -Path ($FilePath -f $Format) | Format-Table

# xml
$Format = 'xml'
$Resources | 
    ConvertTo-Xml | 
    Export-Clixml -Path ($FilePath -f $Format) -Encoding UTF8

Write-host "Format[$Format]:" -BackgroundColor DarkGreen
Import-Clixml -Path ($FilePath -f $Format) 