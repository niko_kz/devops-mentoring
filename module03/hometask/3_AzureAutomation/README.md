# Hometask 03

## Description

- Create a runbook to stop all VMs in a subscription or Resource group.
- Create a runbook to start all VMs in a subscription or Resource group.
- Create a Schedule for runbooks.

---

## General

Plans of the automation account:
  - `Free`
    - Job run time: 500 minutes per m
    - Watchers: 744 hours per m
    - Update management : N/A
    - Configuration management : N/A, but 5 `non-azure` nodes
  - `Basic` : 
    - Job run time: 0.002/minute
    - Watchers: 0.002/hour
    - Configuration management : Free, but $6/`non-azure` node
    - Update management: Free


---
## Assets

To share information accross runbooks in the same account.

### Management Certificates

### Assets

  * Credentials
  * Connections
  * Schedules
  * Varibles
  * Modules

#### Varialbes

Стандартизовать и централизовать частые параметры.

To avoid hard codding values in their code

#### Modules

Modules are not runbooks

30 megabyte limit

Not all powershell modules can be imported

#### Credentials

Certificate Credemtials (Maps to management certificate)
Windows Powershell Credentials (username and passowrd)

#### Connections

Databases
Networks
External systems