# Create a runbook to stop/start all VMs in a subscription or Resource group
workflow module03-task01
{
  Param (
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String] $ResourceGroupName,

    [Parameter(Mandatory=$true)]
    [ValidateSet("Start", "Stop")]
    [String] $Action
  )

  # Just the simpliest way to connect (RunAsAccount)
  Write-Output "Connecting to Azure"
  $Connection = Get-AutomationConnection -Name 'AzureRunAsConnection'
  $Sertificate = Get-AutomationCertificate -Name 'AzureRunAsCertificate'
  
  Add-AzureRmAccount  -ServicePrincipal `
                      -Tenant $Connection.TenantID `
                      -ApplicationId $Connection.ApplicationID `
                      -CertificateThumbprint $Sertificate.Thumbprint

  Write-Output "Getting Resource Group $ResourceGroup"
  $ResourceGroup = Get-AzureRmResourceGroup -Name $ResourceGroupName

  Write-Output "Getting VMs"
  $AzureVMs = $ResourceGroup | Get-AzureRMVm

  foreach -parallel ($AzureVM in $AzureVMs)
  {
    Write-Output ("Performing '{0}' operation on '{1}' at '{2}' resource group" -f $Action, $AzureVM.Name, $ResourceGroupName)

    if ($Action -eq "Stop")
    {
      $AzureVM | Stop-AzureRmVM -Force
    } 
    elseif ($Action -eq "Start") 
    {
      $AzureVM | Start-AzureRmVM
    } 
    else 
    {
      throw "$Action? It's wrong. How? There is validationset. Are you hacker?"
    }
  } 
  Write-Output "Runbook done." 
}