<#
 .SYNOPSIS
 Create ResurceGroup and deploy ARM Template

 .PROGRAM
 DevOps Mentoring Program. Azure Stream #3 (KZ)

 .MODULE 03
 Azure PowerShell

 .TASK

 .AUTHOR
 Nikolay Kozhemyak
#>

$ResourceGroupName     = "module-03-rg"
$ResourceGroupLocation = "eastus"

New-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation

New-AzureRmAutomationAccount -ResourceGroupName $ResourceGroupName `
                             -Name "module-03-aa" `
                             -Location "eastus2"

# Plan:
#   Free  - 
#   Basic - 


# RUNBOOKS

    # IMPORT
    # You can use Azure part of the MS Script Center or Gallery

# AUTHOR A RUNBOOK

    # AUTHOR MODE
    # Create any type of asset
    # import module
    # Modules - Activities


# Assests


Connect-AzureRmAccount

if (condition) {
  
} elseif (condition) {
  
} else {
  
}