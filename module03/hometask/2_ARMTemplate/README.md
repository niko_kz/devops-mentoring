# Hometask 02

## Description

- Create a template and add tags to a few resources in a subscription.
- PowerShell Remote: access your VM and read IIS logs, first lines of log file, last lines.

---
## Create a template

Add TAGS to the resources.

```json
"tags": {
  "displayName": "StorageAccount",
  "program": "EPAM Mentoring",
  "module": "3"
}
```
Deploy the deployment group.

```Powershell
New-AzureRmResourceGroupDeployment  -Name "module-03-deployment" `
                                    -ResourceGroupName $ResourceGroupName `
                                    -TemplateFile $TemplateFile `
                                    -TemplateParameterFile $TemplateParametersFile `
                                    -Verbose -Force
```

## PowerShell Remote
---
### Easy Solution (No Secure)

```Powershell
Enable-PSRemoting -Force

Set-Item wsman:\localhost\Client\TrustedHosts
Get-Item wsman:\localhost\Client\TrustedHosts

$cred = Get-Credential
Enter-PSSession -ComputerName module03-vm-test.eastus.cloudapp.azure.com -Port 5985 -Credential $cred
```

---
### Complex Solution (More Secure)
[Link](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/winrm) to documentation.

#### Step 1: Create a Key Vault

```Powershell
New-AzureRmKeyVault -VaultName "<vault-name>" `
                    -ResourceGroupName "<rg-name>" `
                    -Location "<vault-location>" `
                    -EnabledForDeployment `
                    -EnabledForTemplateDeployment
```

#### Step 2: Create a self-signed certificate

```Powershell
$certificateName = "somename"

$thumbprint = (New-SelfSignedCertificate -DnsName $certificateName -CertStoreLocation Cert:\CurrentUser\My -KeySpec KeyExchange).Thumbprint

$cert = (Get-ChildItem -Path cert:\CurrentUser\My\$thumbprint)

$password = Read-Host -Prompt "Please enter the certificate password." -AsSecureString

Export-PfxCertificate -Cert $cert -FilePath ".\$certificateName.pfx" -Password $password
```

#### Step 3: Upload your self-signed certificate to the Key Vault

```Powershell
$fileName = "<Path to the .pfx file>"
$fileContentBytes = Get-Content $fileName -Encoding Byte
$fileContentEncoded = [System.Convert]::ToBase64String($fileContentBytes)

$jsonObject = @"
{
  "data": "$filecontentencoded",
  "dataType" :"pfx",
  "password": "<password>"
}
"@

$jsonObjectBytes = [System.Text.Encoding]::UTF8.GetBytes($jsonObject)
$jsonEncoded = [System.Convert]::ToBase64String($jsonObjectBytes)

$secret = ConvertTo-SecureString -String $jsonEncoded -AsPlainText –Force
Set-AzureKeyVaultSecret -VaultName "<vault name>" -Name "<secret name>" -SecretValue $secret
```

#### Step 4: [Get the URL for your self-signed certificate in the Key Vault](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/winrm#step-4-get-the-url-for-your-self-signed-certificate-in-the-key-vault)

```json
"certificateUrl": "[reference(resourceId(resourceGroup().name, 'Microsoft.KeyVault/vaults/secrets', '<vault-name>', '<secret-name>'), '2015-06-01').secretUriWithVersion]"
```

```Powershell
$secretURL = (Get-AzureKeyVaultSecret -VaultName "<vault name>" -Name "<secret name>").Id
```

#### Step 5: Reference your self-signed certificates URL while creating a VM

```json
"osProfile": {
      ...
      "secrets": [
        {
          "sourceVault": {
            "id": "<resource id of the Key Vault containing the secret>"
          },
          "vaultCertificates": [
            {
              "certificateUrl": "<URL for the certificate you got in Step 4>",
              "certificateStore": "<Name of the certificate store on the VM>"
            }
          ]
        }
      ],
      "windowsConfiguration": {
        ...
        "winRM": {
          "listeners": [
            {
              "protocol": "http"
            },
            {
              "protocol": "https",
              "certificateUrl": "<URL for the certificate you got in Step 4>"
            }
          ]
        },
        ...
      }
    },
```

```Powershell
$vm = New-AzureRmVMConfig -VMName "<VM name>" -VMSize "<VM Size>"
$credential = Get-Credential
$secretURL = (Get-AzureKeyVaultSecret -VaultName "<vault name>" -Name "<secret name>").Id
$vm = Set-AzureRmVMOperatingSystem -VM $vm -Windows -ComputerName "<Computer Name>" -Credential $credential -WinRMHttp -WinRMHttps -WinRMCertificateUrl $secretURL
$sourceVaultId = (Get-AzureRmKeyVault -ResourceGroupName "<Resource Group name>" -VaultName "<Vault Name>").ResourceId
$CertificateStore = "My"
$vm = Add-AzureRmVMSecret -VM $vm -SourceVaultId $sourceVaultId -CertificateStore $CertificateStore -CertificateUrl $secretURL
```

#### Step 6: Connecting to the VM

```Powershell
# Check local configuration
Enable-PSRemoting -Force

# Once the setup is done, you can connect to the VM
Enter-PSSession -ConnectionUri https://<public-ip-dns-of-the-vm>:5986 `
                -Credential $cred `
                -SessionOption (New-PSSessionOption `
                -SkipCACheck `
                -SkipCNCheck `
                -SkipRevocationCheck) `
                -Authentication Negotiate
```

---
### Read Logs

```Powershell
# https://blogs.technet.microsoft.com/rmilne/2016/06/03/powershell-tail-command/

Get-EventLog -LogName Application -Newest 10
Get-Content -Path '' -Tail
Get-Content -Path '' -Tail -wait
Get-Content -Path '' -ReadCount 1
```