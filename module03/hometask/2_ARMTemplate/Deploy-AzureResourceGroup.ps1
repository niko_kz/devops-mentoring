<#
 .SYNOPSIS
 Create ResurceGroup and deploy ARM Template

 .PROGRAM
 DevOps Mentoring Program. Azure Stream #3 (KZ)

 .MODULE 03
 Azure PowerShell

 .TASK
 Create a template and add tags to a few resources in a subscription.

 .AUTHOR
 Nikolay Kozhemyak
#>

#Requires -Version 3.0
Set-StrictMode -Version 3
$ErrorActionPreference = 'Stop'

# -----------------------------------------------------------------------------
# Variables
$ServicePrefix          = "module-03"
$ResourceGroupName      = "$ServicePrefix-rg"
$ResourceGroupLocation  = "eastus"
$TemplateFile           = Join-Path $PSScriptRoot "ARMTemplate.json"
$TemplateParametersFile = Join-Path $PSScriptRoot "ARMTemplate.parameters.json"

# -----------------------------------------------------------------------------
# Don't forget to do connect-azurermaccount before

# -----------------------------------------------------------------------------
# Resource Group

New-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation -Verbose -Force

# -----------------------------------------------------------------------------
# Deployment Group

$DeploymentGroupName = "$ServicePrefix-deployment"

New-AzureRmResourceGroupDeployment  -Name $DeploymentGroupName `
                                    -ResourceGroupName $ResourceGroupName `
                                    -TemplateFile $TemplateFile `
                                    -TemplateParameterFile $TemplateParametersFile `
                                    -Verbose -Force

# -----------------------------------------------------------------------------