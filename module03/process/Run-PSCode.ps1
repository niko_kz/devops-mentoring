#################################################
# Managing Azure IaaS with PowerShell
# Introducing IaaS and Azure Powershell
#################################################
# Variables
$Prefix = 'module03'
$ResourceGroup = "${Prefix}-rg"
$StorageAccount = "${Prefix}sa"
$Location = "eastus"

$Tags = @{
    'Reason' = 'DevOps Mentoring'
    'Module' = 3
    'Cource' = 'Managing Azure IaaS with PowerShell'
}

# Connect-AzureRmAccount

#################
# Prerequisites #
#################
# Resource Group
New-AzureRmResourceGroup -name $ResourceGroup -Location $Location -Tags $Tags

# Storage Account
New-AzureRmStorageAccount -Name $StorageAccount -SkuName Premium_LRS `
                          -ResourceGroupName $ResourceGroup -Location $Location 

###########
# Network #
###########
# Subnet
$Subnet = New-AzureRmVirtualNetworkSubnetConfig -Name "${Prefix}-FrontEndSubnet" `
                                                -AddressPrefix 10.0.0.0/24

# VNet
$VNet = New-AzureRmVirtualNetwork -Name "${Prefix}-vnet" -ResourceGroupName $ResourceGroup `
                                  -Location $Location -AddressPrefix 10.0.0.0/16 -Subnet $Subnet

# Nics for VM
$pip = New-AzureRmPublicIpAddress -Name "${Prefix}-pip" -AllocationMethod Dynamic `
                                  -ResourceGroupName $ResourceGroup -Location $Location

$Nic = New-AzureRmNetworkInterface -Name "${Prefix}-nic" -SubnetId $VNet.Subnets[0].Id `
                                   -PublicIpAddressId $pip.Id -ResourceGroupName $ResourceGroup -Location $Location

###################
# Virtual Machine #
###################

$VM = New-AzureRmVMConfig -VMName "${Prefix}-win-web01" -VMSize "Standard_B2s"

$Cred = Get-Credential

Set-AzureRmVMOperatingSystem -VM $VM `
                             -Windows `
                             -ComputerName 'win-web01' `
                             -Credential $Cred `
                             -ProvisionVMAgent `
                             -EnableAutoUpdate

Set-AzureRmVMSourceImage -VM $VM `
                         -PublisherName "MicrosoftWindowsServer" `
                         -Offer "WindowsServer" `
                         -Skus "2012-R2-Datacenter" `
                         -Version "Latest"

Add-AzureRmVMNetworkInterface -vm $VM -Id $Nic.Id


# VHD

$SA = Get-AzureRmStorageAccount -ResourceGroupName $ResourceGroup -Name $StorageAccount

$OSDiskURI = $SA.PrimaryEndpoints.Blob.ToString() + "vhds/" + "os-disk" + ".vhd"

Set-AzureRmVMOSDisk -VM $VM -Name "os-disk" -VhdUri $OSDiskURI -CreateOption fromImage


# Finally

New-AzureRMVM -ResourceGroupName $ResourceGroup -Location $Location -VM $VM -AsJob

###################

## VMCONFIG : Name, Size
## VMOS : Credential, Agent, EnabrlAutoUpdate, Windows

#################################################
# Managing Azure IaaS with PowerShell
# CUstom images and load balancers
#################################################



#################################################
# Managing Azure IaaS with PowerShell
# Powershell DSC & ARM Templates
#################################################

# ARM Templates are just JSON Files.

# DSC

    # DSC Extension

    # Linux, run localy to produce MOF file

# ARM Templates

    # Don't specify how to deployment, only desire

    # Visual Studio

    # Quck Templates

    # ��������� �� �������������

#################################################
# Managing Azure IaaS with PowerShell
# Manging Azure VMs 
#################################################

# Securing Resource

    # Credentials
        
        # ����� �� ����� ������ �����, ������������ ������������ ������, �������� ������ ������, � ����� ���������.

        Set-AzureRmVMAccessExtension

        Get-AzureRmNetworkInterface
        New-AzureRmPublicIpAddress 
        Set-AzureRmNetworkInterface -NetworkInterface $nic

    # Patching

        # AV Helps to 

            DSC xHotfix

        # Linux 
            # OsPathingForLinux Extension

    # NSG
        # Nic, Subnet, 

        # Method 1 : Allow incomming ports and Deny All outgoing ports

        200 rules for NSG
        100 NSGs for Subscription
        

# Perfomance

    # VM Size

        Basic_A1
            
            A1 - Size 1..8

            Basic - Service Level (Basic, Standard, Premium)

            A - Series (SKU) : Hardware Type, Functionality, Perfomance

        Standard_A1



            # Basic:  Cant' use AV, Load Balancing, Limited Disk performance
                    Disk to 300 IOPS

            Standard: All Functions, 500 IOPS

            Premium: 5000 IOPS

        Storage Accounts Limits: 20000 IOPS, 50GB/s


    # Scalling UP and Down

        # WIthin the SKU (A, B, D ...)

        $VM = Get-AzureRmVM
        
        $VM.HardwareProfile.VmSize = 'NEW OR OLD'

        Update-AzureRmVM -vm $VM

        # CHange SKU

        Stop-AzureRmVM

            # The same next
        
        # With D series you can go from 1 core to 16 cores without rebooting systems.


    # Disks
        
        # OS Disks (NBed to shutdown)

            $VM.StorageProfile.OsDisk.DiskSizeGB # max 1023 GB

            # Good practice to create 1000 size, cause you pay only for useble size

        # Data Disk (No need to shutdown)

            Add-AzureRmVMDataDisk -Name 
                -VhdUri 
                -Lun 
                -Caching ReadOnly # ReadOnly - less perf, but more secure. 
                -DiskSizeInGb 1023
                -CreateOption empty

             Update-AzureRmVM

             # After you need to format it locally
   
# MOnitoring

    
    Need to install the extension

    Performance Counters

    System Logs
        Linux Syslog for Linux
        Widnwos Event 

        Retention Strategy


    Windows
        
        Set-AzureRmVMDiagnosticsExtension -DiagnosticsConfigurationPath "PATH TO XML" -StorageAccountName




    Linux

