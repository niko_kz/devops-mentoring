﻿###########################################################
#
# Working with CSV Data in Powershell
#
###########################################################
# CSV
# Comma
# You know the columns, format, amount of properties

#Get-Process | Select-Object Name, Path, Company | Export-Csv -NoTypeInformation -Delimiter ';' -Encoding UTF8
#Import-csv 

###########################################################
# XML
# Dymanic properties




###########################################################
# TXT
# List with single property




###########################################################
# JSON

$Arrays = @{
    'Name' = 'Nikolay'
    'Reason' = 'JSON Import'
    'something' = @(1,2,3,4,5)
    'test' = @{
        'asdasd' = 'asdsad'
        'sasdsdf' = '222'
    }
}


$Arrays  | ConvertTo-Json | out-file 1.txt

$OBJs = Get-Content .\1.txt | ConvertFrom-Json

$OBJs

###########################################################
# HTML
#break
#Get-Process | Select-Object Name, Path, Company | ConvertTo-Html


